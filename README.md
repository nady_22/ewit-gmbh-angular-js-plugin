# EWIT Angular JS Google Maps plugin for Website..


Logic.js  - The main file explained

- Includes the logic to load CSV file from folder 
- Starts Geocoding local Address with a delay (13 requests limitation from Google). 
- The request are sent in chunks with a delay and callback.
- Successful Addresses are rendered on map (with name markers) and grouped based on nearest locations.
- The markers are updated asynchronously after every chunk completes and  requests are succesfull.
- The algorithm Updates clustering algorithm technique and charting APIs dynamically to be shown in home page.

Technology Used: Angular JS + Twitter bootstrap 3.0 + CSS3
APIs : Google Maps V3 + Google ClusterMarkers V2

Features Completed:

- draw a map with a coloured pin (color code is specified on the file) of every location, the name of the person must be shown up when hovering the cursor over the pin.- clustering_multiple_cities_2_MouseOver.jpg
- draw a map with proportional markers (areas with many pins will be grouped into bigger markers) - clustering_multiple_cities_1.jpg
- draw a Region GeoCharts, colouring every country depending on how many elements it contains - GeoChart_of_100_Cities_Loaded_Dynamically_ and 
- draw a pie chart with the number of elements per postal code - 
PieChart_of_100_Cities_Loaded_Dynamically_With
- Folder contains CSS, JS and Index.html 
- Screenshots are attached below

![Home_screen_on_Toggle.jpg](https://bitbucket.org/repo/kdMBAE/images/386316413-Home_screen_on_Toggle.jpg)

![clustering_multiple_cities_2_MouseClick_Name.jpg](https://bitbucket.org/repo/kdMBAE/images/1438572025-clustering_multiple_cities_2_MouseClick_Name.jpg)

![clustering_multiple_cities_2_MouseOver.jpg](https://bitbucket.org/repo/kdMBAE/images/140226076-clustering_multiple_cities_2_MouseOver.jpg)

![Home_screen_on_Toggle.jpg](https://bitbucket.org/repo/kdMBAE/images/2301280316-Home_screen_on_Toggle.jpg)

![PieChart_of_100_Cities_Loaded_Dynamically_With_Control_Panel.jpg](https://bitbucket.org/repo/kdMBAE/images/4048532217-PieChart_of_100_Cities_Loaded_Dynamically_With_Control_Panel.jpg)

![GeoChart_of_100_Cities_Loaded_Dynamically_Without_Control_Panel.jpg](https://bitbucket.org/repo/kdMBAE/images/764676023-GeoChart_of_100_Cities_Loaded_Dynamically_Without_Control_Panel.jpg)

![GeoChart_of_100_Cities_Loaded_Dynamically_With_Control_Panel.jpg](https://bitbucket.org/repo/kdMBAE/images/3562126546-GeoChart_of_100_Cities_Loaded_Dynamically_With_Control_Panel.jpg)

![Control_Panel_Charts_optional..jpg](https://bitbucket.org/repo/kdMBAE/images/2446501563-Control_Panel_Charts_optional..jpg)

![Logs_Cum_Scopes_Monitoring_Options_Available.jpg](https://bitbucket.org/repo/kdMBAE/images/2262181751-Logs_Cum_Scopes_Monitoring_Options_Available.jpg)

Future work: The code is modular and extendable,  For contribution to open community, I look forward to make this as a plugin / widget to CMS systems like Joomla / Django / magento / openCart & Presta shops for others to use 
--
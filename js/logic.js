	"use strict";

		/*We need to manually start angular as we need to
		wait for the google charting libs to be ready*/  
		
		/* Onload Function in Angular JS */
		(function init() {
			// load data, init scope, etc.
			//$scope.isStarted = $scope.isStarted + "Project now initialized";
			google.setOnLoadCallback(function () {  
			angular.bootstrap(document.body, ['googleCSV']);
		});
		})();
		
		google.load('visualization', '1', {packages: ['corechart']});
		var app = angular.module('googleCSV', ['clustered.map','pie-chart','geo-chart']);
		var testSwitch = false;
		
		app.controller('mainCtrl', function($scope,$q, $http,$timeout,$interval){
		 // ======= Global variable to remind us what to do next
		 $scope.contents = {};  //all objects
		 var mapObj;
		 var counter = 0;
		 $scope.msg = '';
		 var delay = 100;
		 var markersData = [];
		 $scope.locationData = [];
		 $scope.test = [];
		 $scope.countries = []; 
		 $scope.countryArrays = [];
		 $scope.ccounter = 0;
		 $scope.cityCounter = 0;
		 $scope.cities = []
		 $scope.countryNames = [];
		 var clusterData;
		 //Map control data
		  $scope.clusterOptions = [{
				"id": 10,
				"group": "Group 0",
				"name": "Markers 10"
			},{
				"id": 50,
				"group": "Group 1",
				"name": "Markers 50"
			},{
				"id": 100,
				"group": "Group 2",
				"name": "Markers 100"
		 },{
				"id": 500,
				"group": "Group 3",
				"name": "Markers 500"
		 },{
				"id": 1000,
				"group": "Group 4",
				"name": "Markers 1000"
		 },{
				"id": 2000,
				"group": "Group 5",
				"name": "Markers 2000"
		 }];
		 
		 // Initial chart data settings.
		$scope.chartTitle = "Location Maps";
		$scope.chartWidth = 800;
		$scope.chartHeight = 600;
		$scope.chartData = [ ];
		
		// ====== Create map objects ======
		var infowindow = new google.maps.InfoWindow();
		//Setting an Initial default value location for the Map.
		var latlng = new google.maps.LatLng(51.5167, 9.9167);
		var mapOptions = {
		zoom: 8,
		center: latlng,
		mapTypeId: google.maps.MapTypeId.ROADMAP
		}

		var geo = new google.maps.Geocoder(); 
		$scope.bounds = new google.maps.LatLngBounds();

		/* markers array here is called test */
		$scope.test = [];

		/* page controls */
		var zoom = parseInt(document.getElementById('zoom').value, 10);
		var size = parseInt(document.getElementById('size').value, 10);
		//default selectedId (i.e 10 markers)
		$scope.clusterOptionID = $scope.clusterOptions[0].id;
		$scope.zoom = zoom == -1 ? null : zoom;
		$scope.mSize = size == -1 ? null : size;
		 
		 //Additional chart options to add / remove / list values.
		 $scope.deleteRow = function (index) {
			$scope.chartData.splice(index, 1);
		};
		$scope.addRow = function () {
			$scope.chartData.push([]);
		};
		$scope.selectRow = function (index) {
			$scope.selected = index;
		};
		$scope.rowClass = function (index) {
			return ($scope.selected === index) ? "selected" : "";
		};
		
		/* default chart variables */
		// activateChart flips to true once the Google 
		// Loader callback fires
		$scope.activateChart = false;
		$scope.result = "Waiting";
		$scope.start = function(){
			$scope.parseData();
		};$timeout($scope.start);
		$interval(callAtInterval, 10000);

		/* Loading the CSV file automatically from folder */
		$scope.parseData = function(){
			$http.get('js/random_addr_file.csv',{cache: true})
			.success (function(data){
				Papa.parse(data, {
					  header: true,
					  worker: true,
					  complete: function(results) {
					  $scope.contents = results.data;
					  $scope.getAddress($scope.mSize);
					}
				});
			});
		};

		// Our promise function with its delay as argument
		var getPromise = function(delay) {
			// Creates a Deferred object
			var deferred = $q.defer();
			// Home page Loader to show time taken to GeoCode Addresses via Google APIs
			var notifications = 0;
			// For each second in the delay, we will notify the caller
			for (var i = 0; i <= delay / 1000; i++) {
				$timeout(function() {
					deferred.notify(notifications++);
				}, i * 1000);
			}
			
			// Always resolve the promise when the delay is reached        
			$timeout(function() {
				deferred.resolve("Success");
			}, delay);
			
			// The promise of the deferred task
			return deferred.promise;
		}
		
		// Service creates a promise that will last 10 seconds
		getPromise(10000).then(function(value) {
			if(testSwitch)
				console.log('promise called !!');
			//call for re-creating the markers.
			$scope.createMarkers();
			$scope.result = value; // Success callback
		}, null, function(value) {
			$scope.result = "Loading Google";
			$scope.notify = value; // Notify callback called each second
		});
		

		function clearClusters(e) {
			e.preventDefault();
			e.stopPropagation();
			markersData.clearMarkers();
		}
		  
		  /* Reset function not added */ 
		function resetMap(e) {
			e.preventDefault();
			e.stopPropagation();
			$scope.clear();
			if (markersData) {
			  markersData.clearMarkers();
			}
			$scope.createMarkers ();
		}
			
		$scope.changedValue = function(newSize) {
		 $scope.mSize = newSize;
		 $scope.getAddress($scope.mSize);
		};
		
		function callAtInterval() {
			if(($scope.mSize-1) != $scope.locationData.length)
			$scope.createMarkers ();
		}
		
		// ====== Geocoding all objects ======
		$scope.getAddress = function(size){
			if(testSwitch)
				console.log('start initializing stuff with parseData!!');
			$scope.countryNames = [];
			$scope.chartData = [];
			$scope.test = [];
			var markersData = [];
			$scope.locationData = [];
			
			var len = $scope.contents.length;
			var deferred = $q.defer();
			var promise = deferred.promise;
			promise.then(function () {
			for (var i = 0; i < len; i++) {
			  if (i === size - 1) {
				//Because it was asynch
				$scope.$watch('locationData', function() {
					if($scope.locationData.length === size - 1){
					// Call to markers service here
					$scope.createMarkers ();
					}
					}, true);
				break;
			  }
			  counter = counter + 1;
			  //assing a scope to each city object for manipulation.
			  mapObj = $scope.contents[i];
			  $scope.geocoder(mapObj, counter);   
			}
			}).then(function () {
				if(testSwitch)
					console.log("promise completes for looop & broke: ");
				setTimeout(function() {
				if(testSwitch)
					console.log("createMarker callback is called: ");
				markersData = $scope.locationData;
				$scope.createMarkers ();
				},1000); 
			});
			deferred.resolve(); 
		};
		
		$scope.geocoder = function (mapObj, weight){
			var address = mapObj.city+','+mapObj.postal_code;
			if(testSwitch)
				console.log("address passed go google is: " + address);
			var obj = [];
			geo.geocode({"address":address}, function(results, status) {
			if (status == google.maps.GeocoderStatus.OK) {
				if (results[0]) {
				//individual marker longitude / latitude is obtained and put into object.
				var location = results[0].geometry.location;
				var lat      = Math.floor(location.lat()*1000+0.5)/1000;
				var lng      = Math.floor(location.lng()*1000+0.5)/1000;
				var loc = 	new google.maps.LatLng(lat, lng);
				var color = iconWithColor(mapObj.color);
				var desc = mapObj.first_name + ' ' + mapObj.last_name;
				var cityInfo;
				obj = [lat,lng, 1, desc, color ];
				setTimeout(function(){$scope.locationData.push(obj)}, 50);
				//add new items into an initobject.
				/*check repeated countries & count */
				 var initObj =  {};
				 var countryName = getCountry(results);
				 if(countryName){
					 initObj.cname = countryName;
					 initObj.ccount = 1;
				 }
				var extObj = angular.extend({}, mapObj, initObj);
				$scope.countryNames.push(extObj);
				}
				}
				else if (status === google.maps.GeocoderStatus.OVER_QUERY_LIMIT) {    
					setTimeout(function() {
					$scope.geocoder(mapObj, weight);
					},100); 
				}
				else if (status === google.maps.GeocoderStatus.ZERO_LIMIT) {    
					setTimeout(function() {
					$scope.geocoder(mapObj,weight);
					}, 100); 
				}
				else{
				  if(testSwitch)
					console.log("Geocode was not successful for the following reason: " + status);
				  var reason="Code "+status;
				  $scope.msg = 'address="' + address + '" error=' +reason+ '(delay='+delay+'ms)<br>';
				  document.getElementById("result").innerHTML += $scope.msg;
				}
			}); //end-of-function
		};
		
		/*  prototypal inheritance*/
		function keyValue(key, value){
			this.Key = key;
			this.Value = value;
		};
		
		keyValue.prototype.updateTo = function(newKey, newValue) {
			this.Key = newKey;
			this.Value = newValue;  
		};

		String.prototype.contains = function(substr) {
			return this.indexOf(substr) > -1;
		};
		
		function hasValue(obj, key, value) {
			if(obj.hasOwnProperty(key) && obj[key] === value)
			return true;
		};
		
		/*sort objects based on countryName */
		function compare(a,b) {
		 if (a.cname < b.cname)
			return -1;
		  else if (a.cname > b.cname)
			return 1;
		  else 
			return 0;
		}

		/* compare if two values are int */
		function compareInt(a, b){
			return b.postal_code-a.postal_code;
		}
		
		/*service to check for unique countryName occurrences */
		function checkCountryData(){
			$scope.a = []; var prev;var b = [];
			//sort all the country names to compare once.
			$scope.countryNames.sort(compare);
			for ( var i = 0; i < $scope.countryNames.length; i++ ) {
				if ( $scope.countryNames[i].cname !== prev ) {
					$scope.a.push($scope.countryNames[i].cname,1);
				} else {
					$scope.a[$scope.countryNames[i].cname, $scope.a.length-1]++;
				}
				prev = $scope.countryNames[i].cname;
			}
			
			var k = 0; var i = $scope.a.length /2 ; var v = 2; $scope.temp = [];
			while (i--){
				if ($scope.a[i]){
				   $scope.temp[i] =   $scope.a.slice(k,v);
				   $scope.chartData.push($scope.temp[i]);
				   k =k+2;v=v+2;
				}
			}
		};
		
		function getCountry(results)
		{
			for (var i = 0; i < results[0].address_components.length; i++)
			{
				var shortname = results[0].address_components[i].short_name;
				var longname = results[0].address_components[i].long_name;
				var type = results[0].address_components[i].types;
				if (type.indexOf("country") != -1)
				{
					if (!isNullOrWhitespace(shortname))
					{
						return shortname;
					}
					else
					{
						return longname;
					}
				}
			}
		}
		
		function isNullOrWhitespace(text) {
			if (text == null) {
				return true;
			}
			return text.replace(/\s/gi, '').length < 1;
		} 
		
		$scope.createMarkers = function(){
			//sometimes data gets delayed
			if($scope.locationData) {
				markersData = $scope.locationData;
			}
			 else{
				markersData = [[51.5167, 9.9167,1],[52.5167, 9.9145,1]];
			} 
			clusterData = markersData.map(function(t) {
				return t
			});
			$scope.test = clusterData;
			$scope.displayRegionsChart();
		};
		
		/*function call to load GeoChart & PieChart data */
		$scope.displayRegionsChart = function() {
			//reset chartData for both maps.
			$scope.chartData = [];
			var countryData = checkCountryData();
		};
		
				/* google chart colors */
		var iconWithColor = function(color) {
			var ccolor = color.replace(/[^a-zA-Z0-9]/g,'');
			var imageUrl = 'http://chart.apis.google.com/chart?cht=mm&chs=24x32&chco='+
			'FFFFFF,'+ccolor+',000000&ext=.png ';
			var markerImage = new google.maps.MarkerImage(imageUrl,
			new google.maps.Size(24, 32));
			return imageUrl;
		  };
		
		
		});//--End of Controller.
		
		/* Beginning of dynamic loading & control of Pie & Geo Charts */
		var pieChart = pieChart || angular.module("pie-chart",[]);
		app.directive('pieChart', function ($timeout) {
		return {
			restrict: 'EA',
			scope: {
				title: '@title',
				width: '@width',
				height: '@height',
				data: '=data',
				dataM : '=test',
				selectFn: '&select'
			},
			link: function ($scope, $elm, $attr) {
				
				// Create the data table and instantiate the chart
				var data = new google.visualization.DataTable();
				data.addColumn('string', 'Label');
				data.addColumn('number', 'Value');
				var chart = new google.visualization.PieChart($elm[0]);
				draw();
				// Watches, to refresh the chart when its data, title or dimensions change
				$scope.$watch('data', function () {
					if(testSwitch)
						console.log('data changed');
					draw(); //redraw chart
				}, true); // true is for deep object equality checking
				$scope.$watch('title', function () {
					draw();
				});
				$scope.$watch('width', function () {
					draw();
				});
				$scope.$watch('height', function () {
					draw();
				});

				// Chart selection handler
				google.visualization.events.addListener(chart, 'select', function () {
					var selectedItem = chart.getSelection()[0];
					if (selectedItem) {
						$scope.$apply(function () {
							$scope.selectFn({
								selectedRowIndex: selectedItem.row
							});
						});
					}
				});	
			
				/*sort objects based on countryName */
				function compare(a,b) {
				 if (a.label < b.label)
					return -1;
				  else if (a.label > b.label)
					return 1;
				  else 
					return 0;
				}
				
				function draw() {
					if (!draw.triggered) {
						draw.triggered = true;
						$timeout(function () {
							draw.triggered = false;
							var label, value;
							//remove old values from Piechart.
							data.removeRows(0, data.getNumberOfRows());
							angular.forEach($scope.data, function (row) {
								label = row[0];
								value = parseFloat(row[1], 10);
								if (!isNaN(value) && value != null) {
									data.addRow([label, value]);
									}
							});
							var options = {
								'title': $scope.title,
									'width': $scope.width,
									'height': $scope.height
							};
							
							chart.draw(data, options);
							// No row selected
							$scope.selectFn({
								selectedRowIndex: undefined
							});
						}, 0, true);
					}
				}
				}
		};
	});
	
	/*start of another directive to control dynamic GeoMaps */
	var geoChart = geoChart || angular.module("geo-chart",[]);
		app.directive('geoChart', function ($timeout) {
		return {
			restrict: 'EA',
			scope: {
				title: '@title',
				width: '@width',
				height: '@height',
				gdata: '=data1',
				dataM : '=test',
				selectFn: '&select'
			},
			link: function ($scope, $elm, $attr) {

				// Create the data table and instantiate the chart
				var geodata = new google.visualization.DataTable();
				geodata.addColumn('string', 'Country');
				geodata.addColumn('number', 'No of cities');
				var gChart = new google.visualization.GeoChart($elm[0]);
				gdraw();
				// Watches, to refresh the chart when its data, title or dimensions change
				$scope.$watch('gdata', function () {
					gdraw();
				}, true); // true is for deep object equality checking
				$scope.$watch('title', function () {
					gdraw();
				});
				$scope.$watch('width', function () {
					gdraw();
				});
				$scope.$watch('height', function () {
					gdraw();
				});

				function gdraw(){
					 $timeout(function () {
							var glabel, gvalue;
							geodata.removeRows(0, geodata.getNumberOfRows());
							angular.forEach($scope.gdata, function (row) {
								console.log($scope.gdata + '$scope.gdata' );
								glabel = row[0];
								gvalue = parseFloat(row[1], 10);
								console.log(gvalue + 'gvalue' );
								if (!isNaN(gvalue) && gvalue != null) {
									geodata.addRow([glabel, gvalue]);
								}
							});
							
							 var geoOptions = {
								'title': 'GeoChart',
									'width': $scope.width,
									'height': $scope.height
							};
							gChart.draw (geodata, geoOptions);
						}, 0, true);
				
				} //end-of-gdraw function
				}
		};
	}); //end-of-geoChart directive.